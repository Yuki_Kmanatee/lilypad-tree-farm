
const int LED_R = 9; //LED が繋がってるピン
const int LED_G = 11;
const int LED_B = 10;

const int LED_W0 = 5;
const int LED_W1 = 6;
const int LED_W2 = 16;
const int LED_W3 = 18;
const int LED_W4 = 17;

const int SLIDE = 2;

const int LIGHT_SENSOR = A6;

void setup() {
  pinMode(LED_R, OUTPUT);
  pinMode(LED_G, OUTPUT);
  pinMode(LED_B, OUTPUT);
  
  pinMode(LED_W0, OUTPUT);
  pinMode(LED_W1, OUTPUT);
  pinMode(LED_W2, OUTPUT);
  pinMode(LED_W3, OUTPUT);
  pinMode(LED_W4, OUTPUT);
  
  pinMode(SLIDE, INPUT);

  pinMode(LIGHT_SENSOR, INPUT);
  
  randomSeed(1);
  Serial.begin(9600);
}

int CHARACTER[30][5] = {
  {1, 0, 0, 1, 0},
  {1, 1, 1, 1, 1},
  {1, 0, 0, 0, 0},
  {0, 0, 0, 0, 0},
  {1, 0, 0, 1, 0},
  {1, 0, 1, 0, 1},
  {1, 0, 1, 0, 1},
  {0, 1, 0, 0, 1},
  {0, 0, 0, 0, 0},
  {1, 1, 1, 1, 1},
  {0, 0, 1, 0, 0},
  {0, 0, 1, 0, 0},
  {1, 1, 1, 0, 0},
  {0, 0, 0, 0, 0},
  {0, 1, 1, 1, 0},
  {1, 0, 0, 0, 1},
  {1, 0, 0, 0, 1},
  {0, 1, 1, 1, 0},
  {0, 0, 0, 0, 0},
  {0, 0, 0, 0, 1},
  {0, 0, 0, 0, 1},
  {1, 1, 1, 1, 1},
  {0, 0, 0, 0, 1},
  {0, 0, 0, 0, 1},
  {0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0}
};

int currentIdx = 0;
boolean isInNightMode = false;
int nightModeBrightCoeff = 0;
boolean isNightModeFadeInFinished = false;

void loop() {

  int lightValue = analogRead(LIGHT_SENSOR);
  //Serial.println(lightValue);
  nightModeLoop(lightValue);
  
  showString();
}


//
// Even when `isInNightMode` is false,
// this function will be called as a initial loop of
// night mode.
void nightModeLoop(int light)
{
  analogWrite(LED_R, 255);
  analogWrite(LED_G, 255);
  analogWrite(LED_B, 255);
  
  if (!isInNightMode && light < 100) 
  {
    // Initiates a night mode.
    nightModeBrightCoeff = 0;
    isNightModeFadeInFinished = false;
    isInNightMode = true;
  }
  if (isInNightMode && light >= 100)
  {
    nightModeBrightCoeff = 0;
    isNightModeFadeInFinished = true;
    isInNightMode = false;
  }
  
  if (isInNightMode && !isNightModeFadeInFinished) {
    if (nightModeBrightCoeff < 255) {
      nightModeBrightCoeff++;
    } else {
      isNightModeFadeInFinished = true;
    }
  }
  
  //Serial.println("isInNightMode:");
  //Serial.println(isInNightMode == true? "true": "false");
  
  
  if (isInNightMode)
  {
    int decreases = random(0, 24);
    //Serial.println((2 * (nightModeBrightCoeff / 2)) - decreases);
    analogWrite(LED_R, min(255 - (nightModeBrightCoeff * 3) + decreases, 0));
    analogWrite(LED_G, 255 - (nightModeBrightCoeff / 2) + decreases);
    analogWrite(LED_B, 255);
    //delay(10);
  } else {
    //Serial.println("Writing 0");
    analogWrite(LED_R, 255);
    analogWrite(LED_G, 255);
    analogWrite(LED_B, 255); 
    //delay(10);
  }
  
}

void showString()
{
  int i = currentIdx;
  digitalWrite(SLIDE, 1);
  int slide = digitalRead(SLIDE);
  Serial.println(slide);
  
  if (slide == false)
  {
  
    // Reset
    digitalWrite(LED_W0, 0);
    digitalWrite(LED_W1, 0);
    digitalWrite(LED_W2, 0);
    digitalWrite(LED_W3, 0);
    digitalWrite(LED_W4, 0);
    
    delay(2);
    
    digitalWrite(LED_W0, CHARACTER[i][0]);
    digitalWrite(LED_W1, CHARACTER[i][1]);
    digitalWrite(LED_W2, CHARACTER[i][2]);
    digitalWrite(LED_W3, CHARACTER[i][3] * 255);
    digitalWrite(LED_W4, CHARACTER[i][4] * 255);
    
    if (currentIdx < 30)
    {
      currentIdx++;
    } else {
      currentIdx = 0;
    }
  } else {
    digitalWrite(LED_W0, 0);
    digitalWrite(LED_W1, 0);
    digitalWrite(LED_W2, 0);
    digitalWrite(LED_W3, 0);
    digitalWrite(LED_W4, 0);
  }
  
  delay(5); 
}
